var gr;

function queryPrepare(queryStr, mysql) {
	return new Promise((resolve, reject) => {
	  try {
	  var qblock = '', qs = 0, qe = 0, qarrx = [], qarr = [], qx = [], cnx = 0, cny = 0, cz, query, modBlock;
	  if(queryStr.ciIndexOf("value") !== -1) {
		qs = queryStr.indexOf("(");
		qe = queryStr.indexOf(")");
		query = queryStr.substr(0, qs);
		qblock = queryStr.substr(qs, (qe + 1));
		qarrx = qblock.split("),");
		qarrx.forEach(function(element) {
		  cnx = 0;
		  element = element.substr((element.indexOf('(') + 1), element.length);
		  if(cny > 0) {
			query += ', (';
		  } else {
			query += '(';
		  }
		  qx = element.split(",");
		  qx.forEach(function(ele) {
			qarr.push(ele.trim());
			if(cnx > 0) {
			  query += ', ?';
			} else {
			  query += '?';
			}
			cnx++;
		  });
		  query += ')';
		});
	  } else if((qs = queryStr.ciIndexOf("where")) !== -1) {
		qs += 5;
		modBlock = this.wh(queryStr, qs);
		query = queryStr.substr(0, qs);
		query += modBlock[0];
		cz = modBlock[2];
		query += queryStr.substr(cz, queryStr.length);
		query = mysql.format(queryStr, modBlock[1]);
	  } else if((qs = queryStr.ciIndexOf("having")) !== -1) {
		qs += 6;
		modBlock = this.wh(queryStr, qs);
		query = queryStr.substr(0, qs);
		query += modBlock[0];
		cz = modBlock[2];
		query += queryStr.substr(cz, queryStr.length);
		query = mysql.format(queryStr, modBlock[1]);
	  } else {
		query = queryStr;
	  }
	  return resolve(query);
		} catch(e) {
		  return reject(e);
		}
	}).then((resolve) => {
	  gr = resolve;
	  return gr;
	}).catch((reject) => {
	  return reject;
	});
  }
module.exports = queryPrepare;
