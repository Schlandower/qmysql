var gr;
function wh(queryStr, x) {
  return new Promise((resolve, reject) => {
	var retval = [], notfound = 999999, qc = [], minimum, tmp;
try {
	if((qc[0] = queryStr.ciIndexOf("group by")) === -1) {
	  qc[0] = notfound;
	} else if((qc[0] + 8) <= x) {
	  qc[0] = notfound;
	}
	if((qc[1] = queryStr.ciIndexOf("having")) === -1) {
	  qc[1] = notfound;
	} else if((qc[1] + 6) <= x) {
	  qc[1] = notfound;
	}
	if((qc[2] = queryStr.ciIndexOf("order by")) === -1) {
	  qc[2] = notfound;
	} else if((qc[2] + 8) <= x) {
	  qc[2] = notfound;
	}
	if((qc[3] = queryStr.ciIndexOf("limit")) === -1) {
	  qc[3] = notfound;
	} else if((qc[3] + 5) <= x) {
	  qc[3] = notfound;
	}
	if((qc[4] = queryStr.ciIndexOf("procedure")) === -1) {
	  qc[4] = notfound;
	} else if((qc[4] + 9) <= x) {
	  qc[4] = notfound;
	}
	if((qc[5] = queryStr.ciIndexOf("into outfile")) === -1) {
	  qc[5] = notfound;
	} else if((qc[5] + 12) <= x) {
	  qc[5] = notfound;
	}
	if((qc[6] = queryStr.ciIndexOf("into dumpfile")) === -1) {
	  qc[6] = notfound;
	} else if((qc[6] + 13) <= x) {
	  qc[6] = notfound;
	}
	if((qc[7] = queryStr.ciIndexOf("where ")) === -1) {
	  qc[7] = notfound;
	} else if((qc[7] + 5) <= x) {
	  qc[7] = notfound;
	}

	minimum = Math.min(...qc);
	if(minimum === notfound) {
	  minimum = queryStr.lastIndexOf(";");
	}

	tmp = queryStr.substr(x, minimum);
	retval[2] = minimum;
	var tmpx = tmp.split(" ");
	var a, b, c = [];
	for(a = 0, b = tmpx.length; a < b; a++) {
	  c = tmpx[a].split("=");
	  retval[1].push(c[0].trim());
	  retval[1].push(c[1].trim());
	  retval[0] += ' ?? = ? ';
	}
	return resolve(retval);
  } catch(e) {
	return reject(e);
  }
  }).then((resolve) => {
	gr = resolve;
	return gr;
  }).catch((reject) => {
	throw reject;
  });
}
module.exports = wh;
