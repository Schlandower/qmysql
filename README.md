'qmysql' is a module built to take a standard PHP style query and create an 'ideal' node.js MySQL / Mariadb query, run the query, and return the result.
Built in is the ability to take an array of queries, run them, and return the combined results.

(This is still in development and only an alpha release!!!)

Usage:
var qmysql = require('qmysql');
qmysql.init(username, password, [[host], [[number of pool connections], [debug]]]);
var result = qmysql.query('select * from mytable');

or

var result = qmysql.query(['select a, b, c, d from mytable0', 'select f, g, h,j, k from mytable1']);

if(Object.keys(result[1]).includes('errno') === false) {
// Returns an array of objects, one for each line.
console.log(JSON.stringify(result[0]));
}

To process each object you can use 'objectforeach'.
Example:
require('objectforeach');
result[0].foreach((row) => {
row.foreach((ovalue, okey) => {
console.log('Key: ' + key + '   Value: ' + value);
});
});
