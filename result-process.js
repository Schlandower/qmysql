var gr;

function resultProcess(result) {
  return new Promise((resolve, reject) => {
	try {
	  var retval;
	  if(Array.isArray(result)) {
		if(typeof result[0] === 'object') {
		  retval = result;
		} else {
		  if(Array.isArray(result[0])) {
			 if(typeof result[0][0] === 'object') {
			retval = result[0];
		  }
		}
	  }
	}
	return resolve(retval);
  } catch(e) {
	return reject(e);
  }
}).then((resolve) => {
  gr = resolve;
  return gr;
}).catch((reject) => {
  throw reject;
});
}
module.exports = resultProcess;
